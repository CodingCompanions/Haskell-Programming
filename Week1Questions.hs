--Code snippents for Week1 
myOr :: Bool -> Bool -> Bool
myOr b1 b2 = False
myOr True b2 = True
myOr b1 True = True

f :: Int -> Int
f n = g n (n+1)

g :: Int -> Int -> Int
g m i
  | (mod i m) == 0 = i
  | otherwise = g m (i+1)

h::Int->Int->Int
h m 0 = m
h m n = h (div m 10) (10*n + (mod m 10))

i::Int->Int->Int->Bool
i m n p = div m n >= p

j::Int->Bool->Int->Bool
j a b x = (mod a x > 0) && b

--data Matrix = Matrix { Matrix :: [[Int]] } deriving (Eq,Read,Show)


is_matrix :: [[a]] -> Bool
is_matrix []   =False
is_matrix [[]] = False
is_matrix ([]:xs) = is_matrix xs
is_matrix ([x]:xs) = True
is_matrix (x:xs)
  | length x == length (head xs) = is_matrix xs
  | otherwise = False
  

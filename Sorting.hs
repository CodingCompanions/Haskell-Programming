{- Insertion Sort :
   COmplexity : O(n^2)
   
   -}
   
--Auxillary Function
insert x []=[x]
insert x (y:ys)
  | (x<=y)=x:y:ys
  | otherwise y:(insert x ys)

--Type1  
iSort []=[]
iSort (x:xs)=insert x (iSort xs)

--Type2
isort =foldr insert []


{- Merge Sort :
  Complexity : O(n logn)
  
  -}
  
merge [] ys=ys
merge xs []=xs

merge (x:xs) (y:ys)
  | x<=y     = x:(merge xs (y:ys))
  | otherwise= y:(merge (x:xs) ys)

  
mergesort [] = []
mergesort [x]= [x]

mergesort l=merge(mergesort (front l)) (mergesort (back l))

  where
  front l=take((length l) `div 2`) l
  back l =drop((length l) `div 2`) l
  
  
{- Quick Sort :
   Complexity : O(n^2)
                Average : O(n logn) so, better :p
   -}
   
quickSort []=[]
quickSort (x:xs)=(quickSort lower)++[splitter]++(quickSort upper)

  where 
  splitter = x
  lower=[y|y<-xs,y<=x]
  upper=[y|y<-xs,y>x ]

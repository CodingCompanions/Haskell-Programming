repl :: String -> String
repl "" = ""
repl (c:cs) = c:c:(repl cs)

remDup :: [Int] -> [Int]
remDup [] = []
remDup (x:xs) = x:(remDup (remove x xs))
  where
  remove :: Int -> [Int] -> [Int]
  remove x [] = []
  remove x (y:ys) 
    | x == y = remove x ys
    | otherwise = y:(remove x ys)

remChamp :: [Int] -> [Int]
remChamp [] = []
remChamp (x:xs)
  | x == (maximum (x:xs)) = xs
  | otherwise = x:(remChamp xs)

remRunnerUp :: [Int] -> [Int]
remRunnerUp [] = []
remRunnerUp [x] = [x]
remRunnerUp (x:xs)
  | x == (secondmax (x:xs)) = xs
  | otherwise = x:(remRunnerUp xs)
  where
  secondmax :: [Int] -> Int
  secondmax l = maximum (remChamp l)

--function to find factorial of a number
factorial::Int->Int
--factorial 0 =1
factorial n
  |n==0 = 1
  |n>0 = n*factorial(n-1)
  |otherwise = factorial(-n)

--otherwise is like the "catch all" situation
--conditions for all cases
--
